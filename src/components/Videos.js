import React, { useState } from 'react'
import Informations from './Informations'
import '../App.css'

function Videos() {
  const apiKeyYoutube = 'AIzaSyCdWDn_Bgt9g4l74NdPBX6qU_9IAtcwPzI' //'AIzaSyC1Sn9Ai_QcsIwZ6XGgYgliDnd_V5DsNv4'
  const urlYoutube =
    'https://www.googleapis.com/youtube/v3/search?part=snippet&'

  const [tag, setTag] = useState('')
  const [videos, setVideos] = useState([])

  let listVideos = []

  const field = document.getElementById('fields')
  const inforField = document.getElementById('informationField')

  const getVideos = async () => {
    const videosResponse = await fetch(
      `${urlYoutube}key=${apiKeyYoutube}&type=video&q=${tag}&maxResults=12`
    )
    const dataVideos = await videosResponse.json()
    setVideos(dataVideos.items)

    readjust()
  }

  // Função responsavel para fazer reajustes nos estilos de alguns elementos
  function readjust() {
    // Ajusta o campo de pesquisa para cima
    field.style.alignItems = 'start'

    // Mostra o conteudo oculto
    inforField.style.visibility = 'visible'
  }

  for (let indice in videos) {
    let info = videos[indice].snippet
    listVideos.push(
      <div key={indice} className="videoContainer">
        <iframe
          width="100%"
          height="280px"
          src={`https://www.youtube.com/embed/${videos[indice].id.videoId}`}
          frameBorder="0"
          allowFullScreen
        ></iframe>
        <p id="mainTitle">{info.title}</p>
        <p id="channelTitle">
          <strong>Channel:</strong> {info.channelTitle}
        </p>
        <p id="description">Description: {info.description}</p>
      </div>
    )
  }

  const getTag = element => {
    if (element.target.getAttribute('id') === 'searchField') {
      setTag(element.target.value)
    }
  }

  return (
    <>
      <div className="container">
        <div id="fields" className="containerFields">
          <input
            type="search"
            id="searchField"
            onChange={element => {
              getTag(element)
            }}
          />
          <input
            type="submit"
            id="searchButton"
            value="Search"
            onClick={getVideos}
          />
        </div>
        <div id="informationField" className="informations">
          {<Informations keyword={tag} />}
        </div>
      </div>

      <div className="videos">{listVideos}</div>
    </>
  )
}

export default Videos
