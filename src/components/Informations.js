import React, { useState } from 'react'
import '../App.css'

import youtube from '../images/youtube.png'
import instagram from '../images/instagram.png'
import facebook from '../images/facebook.png'
import twitter from '../images/twitter.png'

function Informations(props) {
  const apiKeyTicketmaster = 'J8UGX93IWVhGSmPxAGHRsMNxMbyZqFkS' //'7wkQhoBHxvA54DskqmawK56gz4wKPqSr'
  const urlTicket =
    'https://app.ticketmaster.com/discovery/v2/attractions.json?keyword='
    
  const [youtube_url, setUrlYoutube] = useState()
  const [socialMidia, setSocialMidia] = useState([])
  const [images, setImages] = useState([])
  const [segment, setSegment] = useState()
  const [genre, setGenre] = useState()

  const field = document.getElementById('infoField')


  const response = async () => {
    const informationResponse = await fetch(
      `${urlTicket}${props.keyword}&apikey=${apiKeyTicketmaster}`
    )
    const dataInformation = await informationResponse.json()
    
    dataInformation._embedded.attractions.forEach(element => {
      if (element.name.toLowerCase() === props.keyword.toLowerCase()) {
        setSocialMidia(element.externalLinks)
        if ('youtube' in element.externalLinks) {
          setUrlYoutube(element.externalLinks.youtube[0].url)
        } else {
          setUrlYoutube(element.externalLinks.instagram[0].url)
        }
        setImages(element.images)
        setSegment(element.classifications[0].segment.name)
        setGenre(element.classifications[0].genre.name)
      }
    })

    field.style.display = 'block'
  }

  let listSocialMidia = []

  for (let indice in socialMidia) {
    if (indice === 'youtube') {
      listSocialMidia.push(
        <li className="listSocialMidia" key={indice}>
          <a href={socialMidia[indice][0].url} target="_blank"><img className="icon" src={youtube} alt="" /></a>
        </li>
      )
    } else if (indice === 'instagram') {
      listSocialMidia.push(
        <li className="listSocialMidia" key={indice}>
          <a href={socialMidia[indice][0].url} target="_blank"><img className="icon" src={instagram} alt="" /></a>
        </li>
      )
    } else if (indice === 'facebook') {
      listSocialMidia.push(
        <li className="listSocialMidia" key={indice}>
          <a href={socialMidia[indice][0].url} target="_blank"><img className="icon" src={facebook} alt="" /></a>
        </li>
      )
    } else if (indice === 'twitter'){
      listSocialMidia.push(
        <li className="listSocialMidia" key={indice}>
          <a href={socialMidia[indice][0].url} target="_blank"><img className="icon" src={twitter} alt="" /></a>
        </li>
      )
    }
  }

  let image = []

  for (let indice in images) {
    image.push(
      <img className="imagesChannel" src={images[indice].url} alt="imagem" />
    )
  }

  return (
    <>
      <input
        type="submit"
        id="btnMoreInfo"
        className="btnInfo"
        onClick={response}
        value="More Informations"
      />
      <div id="infoField" className="informationsField">
        <h2 className="subtitle">Informations</h2>
        <div className="details">
          <div className="channelImg">
            {image[0]}
            <a href={youtube_url} className="channelName" target="_blank">
              {props.keyword}
            </a>
          </div>
          <div className="socialMidia">
            <div className='moreDetails'>
              <p><strong>Segment:</strong> {segment} </p>
              <p><strong>Genre:</strong> {genre}</p>
            </div>
            <h3 className='titleMidias'>Social Midias</h3>
            <ul className='listMidias'>{listSocialMidia}</ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default Informations
