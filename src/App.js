import React from 'react'
import Videos from './components/Videos'
import './App.css'

function App() {
  return (
    <>
      <h1>Artist Search</h1>
      <Videos />
    </>
  )
}

export default App
