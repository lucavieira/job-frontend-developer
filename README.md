<h1>Basic Tube</h1>

<p>Buscador de bandas e artistas. Listagem de Vídeos, Dados, Redes Sociais, Imagens e entre outras informações.</p>

<h2>Trajetória</h2>
<p>Foi um projeto interessante de desenvolver, no decorrer do desenvolvimento tive alguns desafios para entender bem como as API's funcionavam, porém após alguns testes consegui os primeiros resultados e desse ponto ficou muito mais fácil. Fiquei contente com o resultado, mesmo sabendo que pode ser melhorado, o que pretendo fazer. Além disso tive uma surpresa, percebi um avanço no meu modo de desenvolver, em pouco tempo eu ja obtive resultados, o que antes demoraria um pouco mais para começar a vê-los, sei que posso melhorar ainda mais e pretendo me esforçar e me dedicar para alcançar resultados ainda mais surpreendentes. Procurei organizar o código separando cada funcionalidade, no arquivo Videos contendo apenas as requisições da API do Youtube e no arquivo Informations com as respostas da API do Ticketmaster. Já nos estilos, busquei dar nomes que representavam cada bloco ou elemento.</p>

<h2>Como executar o projeto</h2>

1. Faça um clone do projeto na sua máquina.

$ git clone <https://gitlab.com/lucavieira/job-frontend-developer.git>

2. Acesse a pasta do projeto pelo terminal/cmd

$ cd job-frontend-developer

3. Instale as dependências

$ npm install

4. Execute a aplicação

$ npm start

<h2>Tecnologias utilizadas</h2>

* [ReactJS](https://reactjs.org/)
* [JS](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
* [HTML5](https://developer.mozilla.org/pt-BR/docs/Web/HTML)
* [CSS](https://developer.mozilla.org/pt-BR/docs/Web/CSS)
* [API Youtube](https://developers.google.com/youtube)


<a href="https://github.com/lucavieira"><strong>Lucas Vieira</strong></a>
<div>
  <a href = "mailto:lukasveras14@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/lucas-vieira-dev/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
</div>